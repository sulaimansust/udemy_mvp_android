package com.sulaiman.kha.mvpexampleprojects;

import com.sulaiman.kha.mvpexampleprojects.login.LoginActivityMVP;
import com.sulaiman.kha.mvpexampleprojects.login.LoginActivityPresenter;
import com.sulaiman.kha.mvpexampleprojects.login.User;

import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;


/**
 * Created by sulaimankhan on 3/23/2018.
 */

public class PresenterTest {
    LoginActivityMVP.Model mockModel;
    LoginActivityMVP.View mockView;
    LoginActivityPresenter presenter;

    User user;

    /*
    * The below method will be executed before each test start running
    * */
    @Before
    public void setup() {

        mockModel = mock(LoginActivityMVP.Model.class);
        user = new User("Sulaiman", "Khan");

//        when(mockModel.getUser()).thenReturn(user);

        mockView = mock(LoginActivityMVP.View.class);

        presenter = new LoginActivityPresenter(mockModel);
        presenter.setView(mockView);

    }

   /* @Test

    public void noInteractionWithView() {
        presenter.getCurrentUser();
        verifyZeroInteractions(mockView);
    }*/

   /*
   * This test only check the if path of getCurrentUserMethod on presenter
   *
   * */

    @Test
    public void loadTheUserFromRepoWhileValidUserIsPresent() {
        when(mockModel.getUser()).thenReturn(user);

        presenter.getCurrentUser();

        //Varify model interaction, must interact once with getUser method , otherwise provide error
        verify(mockModel, times(1)).getUser();

        verify(mockView, times(1)).setFirstName("Sulaiman");
        verify(mockView, times(1)).setLastName("Khan");
        verify(mockView, never()).showUserNotAvailable();

    }

   /*
   * This test will check the else path of getCurrentUserMethod on presenter
   *
   * */

    @Test
    public void shouldShowErrorMessageWhenUserisNull() {
        when(mockModel.getUser()).thenReturn(null);

        presenter.getCurrentUser();

        verify(mockModel, times(1)).getUser();

        verify(mockView, never()).setFirstName("Sulaiman");
        verify(mockView, never()).setLastName("Khan");
        verify(mockView, times(1)).showUserNotAvailable();

    }


}
