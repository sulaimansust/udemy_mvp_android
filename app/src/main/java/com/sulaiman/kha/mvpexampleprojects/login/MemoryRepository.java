package com.sulaiman.kha.mvpexampleprojects.login;

/**
 * Created by sulaimankhan on 3/23/2018.
 */

public class MemoryRepository implements LoginRepository {


    private User user;

    @Override
    public User getUser() {

        if (user == null) {
            user = new User("default", "default");
            user.setId(0);
            return user;
        } else {
            return user;
        }
    }

    @Override
    public void saveUser(User user) {
        if (user == null) {
            user = getUser();
        }
        this.user = user;
    }
}
