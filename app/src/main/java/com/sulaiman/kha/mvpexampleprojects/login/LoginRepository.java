package com.sulaiman.kha.mvpexampleprojects.login;

/**
 * Created by sulaimankhan on 3/23/2018.
 */

public interface LoginRepository {

    User getUser();

    void saveUser(User user);
}
