package com.sulaiman.kha.mvpexampleprojects.login;

import javax.annotation.Nullable;

/**
 * Created by sulaimankhan on 3/23/2018.
 */

public class LoginActivityPresenter implements LoginActivityMVP.Presenter {


    @Nullable
    private LoginActivityMVP.View view;
    private LoginActivityMVP.Model model;


    public LoginActivityPresenter(LoginActivityMVP.Model model) {
        this.model = model;
    }

    //<editor-fold desc="LoginActivityMVP.Presenter implementation">
    @Override
    public void setView(LoginActivityMVP.View view) {
        this.view = view;
    }

    @Override
    public void loginButtonClicked() {
        if (view != null) {
            if (view.getFirstName().trim().equals("") || view.getLastName().equals("")){
                view.showInputError();
            } else {
                model.createUser(view.getFirstName(), view.getLastName());
                view.showUserSavedMessage();
            }
        }
    }

    @Override
    public void getCurrentUser() {
        User mUser = model.getUser();

        if(mUser == null) {
            if (view!=null){
                view.showUserNotAvailable();
            }
        } else {
            if (view!=null){
                view.setFirstName(mUser.getFirstName());
                view.setLastName(mUser.getLastName());
            }
        }

    }
    //</editor-fold>

}
