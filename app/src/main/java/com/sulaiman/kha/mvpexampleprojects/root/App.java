package com.sulaiman.kha.mvpexampleprojects.root;

import android.app.Application;

import com.sulaiman.kha.mvpexampleprojects.login.LoginModule;

/**
 * Created by sulaimankhan on 3/23/2018.
 */


public class App extends Application {

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .loginModule(new LoginModule())
                .build();
    }

    public ApplicationComponent getApplicationComponent(){
        return applicationComponent;
    }


}
