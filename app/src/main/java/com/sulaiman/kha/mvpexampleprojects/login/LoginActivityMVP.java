package com.sulaiman.kha.mvpexampleprojects.login;

/**
 * Created by sulaimankhan on 3/23/2018.
 */


/*
*
* Holder for other interfaces to use in project
* */

public interface LoginActivityMVP {
    interface View{

        String getFirstName();
        String getLastName();

        void showUserNotAvailable();
        void showInputError();
        void showUserSavedMessage();

        void setFirstName(String firstName);
        void setLastName(String lastName);

    }

    interface Presenter{
        void setView(LoginActivityMVP.View view);

        void loginButtonClicked();

        void getCurrentUser();
    }

    interface Model {
        void createUser(String firstName, String lastName);

        User getUser();
    }
}
