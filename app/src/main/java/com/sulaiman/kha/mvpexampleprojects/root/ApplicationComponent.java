package com.sulaiman.kha.mvpexampleprojects.root;

import com.sulaiman.kha.mvpexampleprojects.login.LoginActivity;
import com.sulaiman.kha.mvpexampleprojects.login.LoginModule;

import javax.inject.Singleton;

import dagger.Component;
import dagger.Module;

/**
 * Created by sulaimankhan on 3/23/2018.
 */

@Singleton
@Component( modules = {ApplicationModule.class, LoginModule.class})
public interface ApplicationComponent {

    void inject(LoginActivity presenterActivity);

}
